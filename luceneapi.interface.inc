<?php

/**
 * @file
 * Search Lucene API interfaces.
 */

/**
 * Interface for key filter plugins.
 */
interface LuceneapiFilterKeysInterface {

  /**
   * Hook that allows for instantiating the plugin and setting the default
   * arguments through calls to LuceneapiQuery::addFilter().
   *
   * @param string $text
   *   The search keys entered by the user.
   * @param LuceneapiSearcher $searcher
   *   The developers to pass the $searcher object.
   *
   * @return
   *   A LuceneapiFilter object.
   */
  public function setArguments($text, $searcher = NULL);
}

/**
 * Interface for term filter plugins.
 */
interface LuceneapiFilterTermInterface {

  /**
   * Hook that allows for instantiating the plugin and setting the default
   * arguments through calls to LuceneapiQuery::addFilter().
   *
   * @param string $text
   *   The string used to filter the results.
   * @param string|array $field
   *   The field definition or name of the field.
   * @param boolean|NULL $sign
   *   A boolean|NULL value denoting the filter's sign.
   * @param float $boost
   *   The boost factor used to adjust the weight of the filter.
   *
   * @return
   *   A LuceneapiFilter object.
   */
  public function setArguments($text, $field = NULL, $sign = TRUE, $boost = '1.0');
}


